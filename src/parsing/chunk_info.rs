use std::fmt::Display;

use super::chunk_ident::ChunkIdent;

#[derive(Clone, Copy, Debug)]
pub struct ChunkInfo {
    pub ident: ChunkIdent,
    pub offset: u64,
    pub payload_len: u32,
}

impl ChunkInfo {
    pub fn payload_start_offset(&self) -> u64 {
        self.offset + 8
    }
    pub fn payload_end_offset(&self) -> u64 {
        self.payload_start_offset() + self.payload_len as u64
    }
}

impl Display for ChunkInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} (len={}) @0x{:x}",
            self.ident, self.payload_len, self.offset
        )
    }
}
