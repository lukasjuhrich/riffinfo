use std::fmt::Display;

#[derive(Clone, Copy, Debug)]
pub struct ChunkIdent(pub [u8; 4]);

impl Display for ChunkIdent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", std::str::from_utf8(&self.0).unwrap(),)
    }
}
