use std::fmt::Display;
use std::slice::Iter;

use crate::tree::TreeLike;

use super::as_ascii_str;
use super::chunk_info::ChunkInfo;

pub enum Chunk {
    Riff {
        info: ChunkInfo,
        form_type: [u8; 4],
        sub_chunks: Vec<Chunk>,
    },
    List {
        info: ChunkInfo,
        list_type: [u8; 4],
        sub_chunks: Vec<Chunk>,
    },
    Other {
        info: ChunkInfo,
    },
}

fn subchunk_info_string(sub_chunks: &[Chunk], info: &ChunkInfo) -> String {
    if sub_chunks.is_empty() {
        "empty".to_string()
    } else {
        format!(
            "{} subchunks @0x{:x}..0x{:x}",
            sub_chunks.len(),
            info.payload_start_offset() + 4,
            info.payload_end_offset(),
        )
    }
}

fn with_subchunks_info_string(sub_chunks: &[Chunk], type_: &[u8; 4], info: &ChunkInfo) -> String {
    format!(
        "{} ({}): @0x{:x} ({})",
        info.ident,
        as_ascii_str(type_),
        info.offset,
        subchunk_info_string(sub_chunks, info)
    )
}

impl Chunk {
    pub fn stylyzed_identifier(&self) -> String {
        match self {
            Chunk::Riff { info: _, form_type, sub_chunks: _ } => {
                format!("RIFF ({})", as_ascii_str(form_type))
            },
            Chunk::List { info: _, list_type, sub_chunks: _ } => {
                format!("[{}]", as_ascii_str(list_type))
            },
            Chunk::Other { info } => format!("{}", info.ident),
        }
    }
}

impl Display for Chunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Chunk::Riff {
                info,
                form_type: list_or_form_type,
                sub_chunks,
            } | Chunk::List {
                info,
                list_type: list_or_form_type,
                sub_chunks,
            } => {
                write!(f, "{}", with_subchunks_info_string(
                    sub_chunks,
                    list_or_form_type,
                    info
                ))
            }
            Chunk::Other { info } => write!(f, "{info}"),
        }
    }
}

impl<'a> TreeLike<'a, Iter<'a, Chunk>, Chunk> for Chunk {
    fn sub_items(&'a self) -> Option<Iter<'a, Chunk>> {
        match self {
            Chunk::Riff {
                info: _,
                form_type: _,
                sub_chunks,
            }
            | Chunk::List {
                info: _,
                list_type: _,
                sub_chunks,
            } => Some(sub_chunks.iter()),
            Chunk::Other { info: _ } => None,
        }
    }
}
