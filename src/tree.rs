use std::slice::Iter;

use colored::Colorize;

/// Mathematically, a (1 + list(_))-coalgebra.
/// In other words, applies to anything which optionally has an `ExactSizedIterator` of children.
pub trait TreeLike<'a, I, T>
where
    T: 'a + Sized,
    I: ExactSizeIterator<Item = &'a T>,
{
    fn sub_items(&'a self) -> Option<I>;
}

#[derive(Clone, Copy, Debug)]
/// Abstract positions an element can occupy in relation to a range, namely first/middle/last/only.
/// Useful for rendering purposes.
pub enum Position {
    First,
    Middle,
    Last,
    Only,
}

impl Position {
    pub fn from_range_force_last<O>(pos: O, beg: O, end: O) -> Self
    where
        O: Ord,
    {
        use Position::*;
        match (pos <= beg, pos < end) {
            (true, false) => Only,
            (true, _) => First,
            (_, false) => Last,
            _ => Middle,
        }
    }
}

pub fn traverse_tree<'a, T>(
    item: &'a T,
    maxdepth: Option<usize>,
    node_action: &impl Fn(&T, &[Position], usize),
) where
    T: 'a + TreeLike<'a, Iter<'a, T>, T>,
{
    _traverse_tree(item, &Vec::new(), 0, maxdepth, node_action)
}

fn _traverse_tree<'a, T>(
    item: &'a T,
    position: &[Position],
    depth: usize,
    maxdepth: Option<usize>,
    node_action: &impl Fn(&T, &[Position], usize),
) where
    T: 'a + TreeLike<'a, Iter<'a, T>, T>,
{
    node_action(item, position, depth);
    if maxdepth.map(|d| d == 0).unwrap_or(false) {
        return;
    }
    if let Some(sub_items) = item.sub_items() {
        let len = sub_items.len();
        for (i, sub_item) in sub_items.enumerate() {
            _traverse_tree(
                sub_item,
                &[
                    position,
                    vec![Position::from_range_force_last(i, 0, len - 1)].as_slice(),
                ]
                .concat(),
                depth + 1,
                maxdepth.map(|d| d - 1),
                node_action,
            );
        }
    }
}

pub fn colored_unicode_tree_prefix(positions: &[Position]) -> String {
    format!(
        "{}{}{}",
        positions[0..positions.len().saturating_sub(1)]
            .iter()
            .map(|p| match p {
                Position::Last => "   ",
                _ => "│   ",
            }
            .truecolor(120, 120, 120)
            .to_string())
            .collect::<String>(),
        match positions.last() {
            None => "",
            Some(Position::Only) => "└──",
            Some(Position::Last) => "└──",
            _ => "├──",
        }
        .truecolor(120, 120, 120),
        if positions.is_empty() { "" } else { " " }
    )
}
