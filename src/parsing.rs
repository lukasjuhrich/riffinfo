use std::{
    io::{Read, Seek, SeekFrom},
};

use self::{chunk_ident::ChunkIdent, chunk_info::ChunkInfo};
pub use chunk::Chunk;

pub fn as_ascii_str(buf: &[u8]) -> String {
    buf.iter()
        .map(|&c| if c.is_ascii() { c as char } else { '.' })
        .collect::<String>()
}

mod chunk_ident;
mod chunk_info;
mod chunk;

/// Parse a chunk and its potential children.
pub fn parse_chunk<R>(reader: &mut R, info: ChunkInfo) -> std::io::Result<Chunk>
where
    R: Read + Seek,
{
    match info.ident {
        ChunkIdent(x) if &x == b"RIFF" => {
            reader.seek(SeekFrom::Start(info.offset + 8))?;
            Ok(Chunk::Riff {
                info,
                form_type: {
                    let mut buf = [0u8; 4];
                    reader.read_exact(&mut buf)?;
                    buf
                },
                sub_chunks: find_chunks(
                    reader,
                    info.payload_start_offset() + 4,
                    info.payload_end_offset(),
                )?
                .iter()
                .map(|&i| parse_chunk(reader, i))
                .collect::<Result<Vec<Chunk>, _>>()?,
            })
        }
        ChunkIdent(x) if &x == b"LIST" => {
            reader.seek(SeekFrom::Start(info.offset + 8))?;
            Ok(Chunk::List {
                info,
                list_type: {
                    let mut buf = [0u8; 4];
                    reader.read_exact(&mut buf)?;
                    buf
                },
                sub_chunks: find_chunks(
                    reader,
                    info.payload_start_offset() + 4,
                    info.payload_end_offset(),
                )?
                .iter()
                .map(|&i| parse_chunk(reader, i))
                .collect::<Result<Vec<Chunk>, _>>()?,
            })
        }
        _ => Ok(Chunk::Other { info }),
    }
}

/// Follow a consecutive chain of chunks starting at `offset` and extract the relevant `ChunkInfo`s.
pub fn find_chunks<R>(
    reader: &mut R,
    offset: u64,
    max_offset: u64,
) -> std::io::Result<Vec<ChunkInfo>>
where
    R: Read + Seek,
{
    reader.seek(SeekFrom::Start(offset)).expect("Seek failed");
    let mut chunks = Vec::<ChunkInfo>::new();

    while match reader.stream_position() {
        Ok(n) => n < max_offset,
        Err(_) => false,
    } {
        match read_one_chunk(reader) {
            Ok(chunk) => {
                chunks.push(chunk);
            }
            Err(err) => {
                println!(
                    "Warning: error reading chunk at {}: {}. Offset + payload_len is {}",
                    reader.stream_position()?,
                    err,
                    offset + max_offset,
                );
                break;
            }
        }
    }
    Ok(chunks)
}

/// Read one RIFF chunk and advance to end of payload.
fn read_one_chunk<R>(reader: &mut R) -> std::io::Result<ChunkInfo>
where
    R: Read + Seek,
{
    // TODO
    Ok(ChunkInfo {
        offset: reader.stream_position()?,
        ident: {
            let mut buf = [0; 4];
            reader.read_exact(&mut buf)?;
            ChunkIdent(buf)
        },
        payload_len: {
            let mut buf = [0; 4];
            reader.read_exact(&mut buf)?;
            let len = parse_4_le_octets(&buf);
            // advance to end of payload
            reader.seek(SeekFrom::Current(len as i64))?;
            len
        },
    })
}

fn parse_4_le_octets(buf: &[u8; 4]) -> u32 {
    (buf[0] as u32) + ((buf[1] as u32) << 8) + ((buf[2] as u32) << 16) + ((buf[3] as u32) << 24)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_little_endian_parsing() {
        let buf = [0x01, 0x02, 0x03, 0x04];
        assert_eq!(parse_4_le_octets(&buf), 0x0403_0201);
    }
}
