use std::{
    io::BufReader,
    path::{Path, PathBuf},
};

use clap::{ArgEnum, Parser, Subcommand};

mod tree;
use colored::{control, Colorize};
use tree::{colored_unicode_tree_prefix, traverse_tree, Position};

mod parsing;
use parsing::{find_chunks, parse_chunk, Chunk};

#[derive(Parser)]
#[clap(about, version, author)]
struct Args {
    // TODO in the future, use [replace](https://github.com/clap-rs/clap/issues/2836)
    #[clap(short = 'C', help = "alias for `--color=always`")]
    c: bool,
    #[clap(long, arg_enum)]
    color: Option<ColorMode>,

    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    Info {
        filename: PathBuf,
        #[clap(default_value_t = 0)]
        offset: u64,
        end_offset: Option<u64>,
        #[clap(short = 'd', long)]
        maxdepth: Option<usize>,
    },
    Extract {},
}

#[derive(ArgEnum, Clone, Copy)]
enum ColorMode {
    Never,
    Auto,
    Always,
}

fn main() {
    reset_sigpipe();
    let args = Args::parse();
    if args.c {
        control::set_override(true)
    };
    match args.color.unwrap_or(ColorMode::Auto) {
        ColorMode::Always => control::set_override(true),
        ColorMode::Never => control::set_override(false),
        _ => {}
    }
    match args.command {
        Command::Info {
            filename,
            offset,
            end_offset,
            maxdepth,
        } => {
            // TEST SCENARIO: given offset where chunk should be, try to read stuff.
            // print all chunk types w/ len
            let (mut reader, file_len) = load_path(filename).expect("Error reading file");
            let chunk_infos = find_chunks(&mut reader, offset, end_offset.unwrap_or(file_len))
                .expect("Error finding chunks");
            let chunks = chunk_infos
                .iter()
                .map(|&chunk| parse_chunk(&mut reader, chunk))
                .collect::<Result<Vec<_>, _>>()
                .expect("error parsing.");
            for chunk in chunks {
                traverse_tree(&chunk, maxdepth, &render_chunk)
            }
        }
        Command::Extract {} => {
            unimplemented!();
        }
    }
}

fn render_chunk(chunk: &Chunk, positions: &[Position], _depth: usize) {
    println!(
        "{}{}",
        colored_unicode_tree_prefix(positions),
        colored_oneline_chunk(chunk),
    )
}

fn colored_oneline_chunk(chunk: &Chunk) -> String {
    match chunk {
        Chunk::Riff {
            info,
            form_type: _,
            sub_chunks: _,
        }
        | Chunk::List {
            info,
            list_type: _,
            sub_chunks: _,
        }
        | Chunk::Other { info } => {
            format!(
                "{} {}..{} ({} bytes)",
                chunk.stylyzed_identifier().bold(),
                format!("0x{:x}", info.offset).green(),
                format!("0x{:x}", info.offset + info.payload_len as u64).blue(),
                info.payload_len,
            )
        }
    }
}

/// On success, return a reader and a length
pub fn load_path<T: AsRef<Path>>(
    path: T,
) -> Result<(BufReader<std::fs::File>, u64), std::io::Error> {
    let file = std::fs::File::open(path)?;
    let file_len = file.metadata()?.len();
    Ok((BufReader::new(file), file_len))
}

/// [Thank you](https://stackoverflow.com/a/65760807/2443886), BurntSushi5 :-)
#[cfg(unix)]
fn reset_sigpipe() {
    unsafe {
        libc::signal(libc::SIGPIPE, libc::SIG_DFL);
    }
}

#[cfg(not(unix))]
fn reset_sigpipe() {}
