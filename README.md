Riffinfo
===

A small tool to allow for some introspection of RIFF files, like `.avi`, `.wav`, and lots more, given the generality of the format.

Riffinfo's purpose is to give you enough information to extract the relevant chunks for yourself, e.g. with `dd`.
